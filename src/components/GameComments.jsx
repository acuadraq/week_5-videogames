// eslint-disable-next-line object-curly-newline
import React, { useState, useRef, useEffect } from 'react';
import useAuth from '../customHooks/useAuth';
import user from '../user.png';
import Pagination from './Pagination.jsx';
import { postComments } from '../adapters/Fetch';

const GameComments = ({ getComents, id }) => {
  const [login, setLogin] = useAuth();
  const [comments, setComments] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [commentsPerPage] = useState(4);
  const content = useRef();

  const settingComments = async () => {
    const response = await getComents();
    setComments(response);
  };

  const paginate = pageNumber => setCurrentPage(pageNumber);

  const handleSubmit = async e => {
    e.preventDefault();
    const newComment = {
      body: content.current.value,
    };
    await postComments(id, newComment, login.token);
    settingComments();
    content.current.value = '';
  };

  const indexOfLastComment = currentPage * commentsPerPage;
  const indexOfFirstComment = indexOfLastComment - commentsPerPage;
  const currentComments = comments.slice(
    indexOfFirstComment,
    indexOfLastComment
  );

  useEffect(() => {
    let mounted = true;
    settingComments().then(() => {
      if (mounted) {
        setLoading(false);
      }
    });
    return function cleanup() {
      mounted = false;
    };
  }, [getComents]);

  return (
    <>
      <div className="comments__container">
        <h2>Game Reviews</h2>
        <hr />
        <div className="comments__grid">
          <div>
            {loading && <div className="animation__loading"></div>}
            {!loading && (
              <>
                {currentComments.map(comment => (
                  <div key={comment.id} className="comment">
                    <div className="comment__header">
                      <span>
                        <img
                          src={user}
                          width="32"
                          height="32"
                          alt="User Icon"
                        />
                      </span>
                      <span className="comment__user">
                        {comment.user.username}
                      </span>
                    </div>
                    <hr />
                    <p>{comment.body}</p>
                  </div>
                ))}
                <Pagination
                  itemsPerPage={commentsPerPage}
                  totalItems={comments.length}
                  paginate={paginate}
                  currentPage={currentPage}
                />
              </>
            )}
          </div>
          <div className="order-reverse">
            {login === false && (
              <h3>You need to log in in order to post a comment</h3>
            )}
            {login !== false && (
              <div className="comment__box">
                <h3>Welcome {login.username}</h3>
                <form onSubmit={handleSubmit}>
                  <h4>Post a comment</h4>
                  <hr />
                  <label htmlFor="content">Content</label>
                  <textarea
                    ref={content}
                    name="content"
                    id="content"
                    cols="50"
                    rows="10"
                    required
                    placeholder="Content of the comment"
                  ></textarea>
                  <div className="button__container">
                    <button className="button">Post Comment</button>
                  </div>
                </form>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default React.memo(GameComments);
