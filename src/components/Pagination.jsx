import React, { useState, useEffect } from 'react';

// eslint-disable-next-line object-curly-newline
const Pagination = ({ itemsPerPage, totalItems, paginate, currentPage }) => {
  const [pageNumbers, setPageNumbers] = useState([]);

  useEffect(() => {
    const total = Math.ceil(totalItems / itemsPerPage);
    setPageNumbers(Array.from({ length: total }, (_, index) => index + 1));
  }, [totalItems, itemsPerPage]);

  return (
    <ul className="pagination">
      {pageNumbers.map(number => (
        <div key={number} onClick={() => paginate(number)}>
          {currentPage === number ? (
            <li className="currentPage">{number}</li>
          ) : (
            <li key={number}>{number}</li>
          )}
        </div>
      ))}
    </ul>
  );
};

export default Pagination;
