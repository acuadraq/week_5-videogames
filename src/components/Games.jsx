import React from 'react';

const Games = ({ game, handleDetail }) => {
  const defaultImg = 'https://via.placeholder.com/320x400';

  return (
    <>
      <div className="game__card" onClick={() => handleDetail(game.id)}>
        <img
          src={
            game.cover_art !== null
              ? game.cover_art.formats.small.url
              : defaultImg
          }
          alt={game.name}
          height="400"
        />
        <div className="game__content">
          <h3>{game.name}</h3>
          <div className="game__footer flex-class">
            <button href="#">Buy</button>
            <span>${game.price}</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default Games;
