// eslint-disable-next-line object-curly-newline
import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { getGames, getComments } from '../adapters/Fetch';
import GameComments from './GameComments.jsx';
import shop from '../shop.png';
import heart from '../heart.png';
import usePrevious from '../customHooks/usePrevious';

const GameDetail = ({ id }) => {
  const [gameInfo, setGameInfo] = useState({});
  const [loading, setLoading] = useState(true);
  const [total, setTotal] = useState(0);
  const [likes, setLikes] = useState(0);
  const prevTotal = usePrevious(total);
  const defaultImg = 'https://via.placeholder.com/380x500';

  const getComents = useCallback(async () => {
    const commentsApi = await getComments(id);
    return commentsApi;
  }, [id]);

  const getGameInfo = async () => {
    const gameInfoApi = await getGames(id);
    setGameInfo(gameInfoApi);
  };

  const calculateTax = () => total * 0.15 + total;

  const afterTax = useMemo(() => calculateTax(), [total]);

  useEffect(() => {
    let mounted = true;
    getGameInfo().then(() => {
      if (mounted) {
        setLoading(false);
      }
    });
    return function cleanup() {
      mounted = false;
    };
  }, []);

  return (
    <>
      {loading && <div className="animation__loading"></div>}
      {!loading && (
        <>
          <p>
            <span className="font-bold mr-4">
              Previous Total Cost:{' '}
              <span className="font-normal"> ${prevTotal}</span>
            </span>
            <span className="font-bold mr-4">
              Total Cost: <span className="font-normal">${total}</span>
            </span>
            <span className="font-bold">
              After Tax: <span className="font-normal">${afterTax}</span>
            </span>
          </p>
          <div className="game-detail__container">
            <div>
              <h2>{gameInfo.name}</h2>
              <p className="game__info">
                <span className="font-bold">
                  Publisher:
                  {gameInfo.publishers.map(publisher => (
                    <span className="font-normal" key={publisher.id}>
                      {' '}
                      {publisher.name}
                    </span>
                  ))}
                </span>
                <span className="font-bold">
                  Release Year:{' '}
                  <span className="font-normal">{gameInfo.release_year}</span>
                </span>
                <span className="font-bold">
                  Genre:{' '}
                  <span className="font-normal">{gameInfo.genre.name}</span>
                </span>
              </p>
              <p className="likes__container">
                <span>
                  <img
                    src={heart}
                    onClick={() => setLikes(likes + 1)}
                    width="24"
                    height="24"
                    alt="Heart"
                    className="heart__image"
                  />
                </span>
                <span className="likes">{likes}</span>
                <span>({gameInfo.comments.length} reviews)</span>
              </p>
              <div className="img__container">
                <img
                  src={
                    gameInfo.cover_art == null
                      ? defaultImg
                      : gameInfo.cover_art.formats.small.url
                  }
                  alt={gameInfo.name}
                  width="380"
                  height="500"
                  className="game__image"
                />
              </div>
            </div>
            <div className="game__content">
              <span className="price-top">Price match guarantee</span>
              <p className="game__price">${gameInfo.price}</p>
              <div className="free-return">
                <span>15-DAY FREE & EASY RETURNS</span>
                <p>
                  If recieved today, the last day to return this item would be
                  in 15 days
                </p>
              </div>
              <hr />
              <div className="select-div">
                <p>Compatible Plataform(s):</p>
                <select name="platforms" id="platforms">
                  {gameInfo.platforms.map(platform => (
                    <option value={platform.id} key={platform.id}>
                      {platform.name}
                    </option>
                  ))}
                </select>
                <p>Software Format:</p>
                <select name="software" id="software">
                  <option value="1">Physical</option>
                  <option value="2">Digital</option>
                </select>
              </div>
              <button
                onClick={() => setTotal(total + parseFloat(gameInfo.price))}
              >
                <img src={shop} alt="Shopping Cart" width="24" height="24" />
                <span>Add to Cart</span>
              </button>
            </div>
          </div>
          <GameComments getComents={getComents} id={id} loading={loading} />
        </>
      )}
    </>
  );
};

export default GameDetail;
