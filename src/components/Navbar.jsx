import React from 'react';
import useAuth from '../customHooks/useAuth';

const Navbar = ({ changePage }) => {
  const [login, setLogin] = useAuth();

  const logOut = () => {
    localStorage.clear();
    setLogin(false);
    changePage(0);
  };

  return (
    <header>
      <nav className="flex-class">
        <ul className="flex-class">
          <li onClick={() => changePage(0)} className="navbar__title">
            Applaudo Buy
          </li>
          <div>
            <li onClick={() => changePage(0)}>Home</li>
            {login === false && <li onClick={() => changePage(-1)}>Log In</li>}
            {login !== false && <li onClick={() => logOut()}>Log Out</li>}
          </div>
        </ul>
      </nav>
    </header>
  );
};

export default Navbar;
