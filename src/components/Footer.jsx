import React from 'react';

const Footer = ({ changePage }) => (
  <footer>
    <div className="footer__container flex-class">
      <div className="footer__title">
        <h1 onClick={() => changePage(0)}>Applaudo Buy</h1>
      </div>
      <div className="footer__links flex-class">
        <div>
          <h4 onClick={() => changePage(0)}>Home</h4>
        </div>
      </div>
    </div>
  </footer>
);

export default Footer;
