import React, { useState, useEffect } from 'react';
import { getGames } from '../adapters/Fetch';
import Pagination from './Pagination.jsx';
import Games from './Games.jsx';

const GamesList = ({ changePage }) => {
  const [games, setGames] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [gamesPerPage] = useState(8);
  const [msgError, setMsgError] = useState('');

  const getList = async () => {
    try {
      const list = await getGames();
      setGames(list);
    } catch (err) {
      setMsgError('There was an error, reload the page');
    }
  };

  useEffect(() => {
    let mounted = true;
    getList().then(() => {
      if (mounted) {
        setLoading(false);
      }
    });
    return function cleanup() {
      mounted = false;
    };
  }, []);

  const indexOfLastGame = currentPage * gamesPerPage;
  const indexOfFirstGame = indexOfLastGame - gamesPerPage;
  const currentGames = games.slice(indexOfFirstGame, indexOfLastGame);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  const handleDetail = id => changePage(id);

  return (
    <>
      <h2>GAMES AVAILABLE</h2>
      <hr />
      {msgError !== '' && (
        <div>
          <h2>{msgError}</h2>
        </div>
      )}
      {loading && <div className="animation__loading"></div>}
      {!loading && (
        <>
          <div className="games__container">
            {currentGames.map(game => (
              <Games key={game.id} game={game} handleDetail={handleDetail} />
            ))}
          </div>
          <Pagination
            itemsPerPage={gamesPerPage}
            totalItems={games.length}
            paginate={paginate}
            currentPage={currentPage}
          />
        </>
      )}
    </>
  );
};

export default GamesList;
