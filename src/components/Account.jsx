import React, { useState, useRef } from 'react';
import { postLogIn } from '../adapters/Fetch';

const Account = () => {
  const [msgError, setMsgError] = useState('');
  const userInput = useRef();
  const passwordInput = useRef();

  const saveInfo = userInfo => {
    const userLocalInfo = {
      id: userInfo.user.id,
      token: userInfo.jwt,
      username: userInfo.user.username,
    };
    localStorage.setItem('user', JSON.stringify(userLocalInfo));
    window.location.reload();
  };

  const HandleLogin = async e => {
    try {
      e.preventDefault();
      const user = userInput.current.value;
      const password = passwordInput.current.value;
      if (user.trim() === '') {
        setMsgError('User field must be fill');
        return;
      }
      const objectUser = {
        identifier: user,
        password,
      };
      const userInfo = await postLogIn(objectUser);
      saveInfo(userInfo);
    } catch (err) {
      setMsgError('Username or password are incorrect');
    }
  };

  return (
    <div className="form__container">
      <h1>Log In</h1>
      <form onSubmit={HandleLogin}>
        <label htmlFor="user">User</label>
        <input
          ref={userInput}
          type="text"
          name="user"
          id="user"
          placeholder="User"
          required
        />
        <label htmlFor="password">Password</label>
        <input
          ref={passwordInput}
          type="password"
          name="password"
          id="password"
          placeholder="Password"
          required
        />
        <div className="button__container">
          <button className="button">Log In</button>
        </div>
      </form>
      {msgError !== '' && (
        <div className="errorMsgLogin">
          <p>{msgError}</p>
        </div>
      )}
    </div>
  );
};

export default Account;
