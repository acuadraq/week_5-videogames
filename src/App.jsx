import React, { useState } from 'react';
import Navbar from './components/Navbar.jsx';
import Footer from './components/Footer.jsx';
import GamesList from './components/GamesList.jsx';
import GameDetail from './components/GameDetail.jsx';
import Account from './components/Account.jsx';
import './styles/styles.scss';

const App = () => {
  const [page, setPage] = useState(0);

  const changePage = id => setPage(id);

  return (
    <>
      <Navbar changePage={changePage} />
      <section>
        <div className="container">
          {page === 0 && <GamesList changePage={changePage} />}
          {page > 0 && <GameDetail id={page} />}
          {page === -1 && <Account />}
        </div>
      </section>
      <Footer changePage={changePage} />
    </>
  );
};

export default App;
