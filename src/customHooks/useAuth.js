import { useState } from 'react';

const getSavedData = () => {
  const savedData = JSON.parse(localStorage.getItem('user'));
  if (savedData) return savedData;
  return false;
};

const useAuth = () => {
  const [value, setValue] = useState(() => getSavedData());

  return [value, setValue];
};

export default useAuth;
