const fetchApi = async (extension, settings) => {
  const request = await fetch(
    `https://trainee-gamerbox.herokuapp.com${extension}`,
    // eslint-disable-next-line comma-dangle
    settings
  );
  const response = await request.json();
  return response;
};

// eslint-disable-next-line no-unneeded-ternary
export const getGames = id => fetchApi(`/games/${id ? id : ''}`);

export const getComments = id => fetchApi(`/games/${id}/comments?_limit=30`);

export const postLogIn = userInfo => {
  const settings = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-type': 'application/json',
    },
    body: JSON.stringify(userInfo),
  };
  return fetchApi('/auth/local', settings);
};

export const postComments = (id, commentObject, token) => {
  const settings = {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
      'Content-type': 'application/json',
    },
    body: JSON.stringify(commentObject),
  };
  return fetchApi(`/games/${id}/comment`, settings);
};
