# Week_5 Videogames

![](public/Screenshot.png)

## Description

This is the weekly assignment for the fifth week. Its a videogames store. This are the main points of the page:

- All of the games are retrieve from an api.
- On the main page we have a list of the games.
- Every game has a detail page with their comments.
- The home page has a pagination by 8 games per page.
- You can log in in order to post comments.
- You can add likes to the games.
- You can add to cart only in that state to see how some hooks works.

If you want to tested in real time use this url: https://week-5-videogames.vercel.app/
